<?php

namespace App\Repository;

use App\Entity\Appointment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Exception\ValidatorException;

/**
 * @method Appointment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Appointment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Appointment[]    findAll()
 * @method Appointment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppointmentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Appointment::class);
    }


    /**
     * retrun Appointments[]|null
     * @return \App\Entity\Appointment[]
     */
    public function getAllAppointments(){
        try{
            $appointments = $this->createQueryBuilder('m')
                ->where('m.status = :status')
                ->setParameter('status', 0)
                ->getQuery()
                ->getResult();

            return $appointments;
        }
        catch (Exception $e){
            throw new Exception('something went wrong');
        }
    }

    /**
     *  return Appointment for the give id or null
     * @param $appointment_id
     * @return Appointment|null
     */
    public function getAppointmentById( $appointment_id ){
        try{
            return $this->find( $appointment_id );
        }
        catch (NotFoundHttpException $e){
            throw new NotFoundHttpException('Item not found');
        }
        catch (Exception $e){
            throw new Exception( 'something went wrong error: '.$e->getMessage() );
        }
    }


    /**
     * create new Appointment
     * @param Request $request
     * @return Appointment|bool
     */
    public function saveAppointment( Request $request ){
        try{
            $startsAt = \DateTime::createFromFormat('Y-m-d H:i:s', $request->request->get('startsAt'));
            $endsAt = \DateTime::createFromFormat('Y-m-d H:i:s', $request->request->get('endsAt'));
            if( !$startsAt || !$endsAt  ){
                return false;
            }
            $appointment = new Appointment();
            $appointment->setTitle($request->request->get('title'));
            $appointment->setStatus(0);
            $appointment->setStartsAt( $startsAt );
            $appointment->setEndsAt(  $endsAt );
            $appointment->setCreatedAt(  new \DateTime( 'now' ) );

            $entityManager = $this->getEntityManager();
            $entityManager->persist( $appointment );
            $entityManager->flush();
            return $appointment;
        }
        catch (Exception $e){
            throw new Exception($e);
        }
    }


    public function updateAppointment ( $appointment_id,  Request $request ){
        try{

            $appointment = $this->find( $appointment_id );
            if( $appointment )
            {
                $request->request->get('title')? $appointment->setTitle($request->request->get('title')): '';
                $request->request->get('startsAt')? $appointment->setTitle($request->request->get('startsAt')): '';
                $request->request->get('endsAt')? $appointment->setTitle($request->request->get('endsAt')): '';

                $entityManager = $this->getEntityManager();
                $entityManager->persist( $appointment );
                $entityManager->flush();
                return $appointment;
            }
            else {
                throw new NotFoundHttpException( 'data not found');
            }
        }
        catch (Exception $e){
            throw new Exception($e);
        }
    }


    public function softDelete( $appointment_id ){
        try{
            $appointment = $this->find( $appointment_id );
            if( $appointment ){
                $appointment->setStatus(1);

                $entityManager = $this->getEntityManager();
                $entityManager->persist( $appointment );
                $entityManager->flush();
                return true;
            }
            else{
                return false;
            }


        }
        catch (Exception $e){
            throw new Exception('something wrong error: '.$e->getMessage());
        }
    }

    public function addOwner(  Request $request ){
        try{
            $appointment = $this->find( $request->request->get('appointmentId') );
            if( $appointment ){
                $appointment->setOwner( $request->request->get('memberId') );
                $entityManager = $this->getEntityManager();
                $entityManager->persist( $appointment );
                $entityManager->flush();
                return $appointment ;
            }
            else {
                return false;
            }

        }
        catch (\Exception $e){
            throw new \Exception('unable to add owner error: '.$e->getMessage() );
        }
    }
}
