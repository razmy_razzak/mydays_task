<?php

namespace App\Repository;

use App\Entity\Member;
use App\Interfaces\MemberInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @method Member|null find($id, $lockMode = null, $lockVersion = null)
 * @method Member|null findOneBy(array $criteria, array $orderBy = null)
 * @method Member[]    findAll()
 * @method Member[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MemberRepository extends ServiceEntityRepository implements MemberInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Member::class);
    }


    /**
     * return member[]
     * @return mixed
     */
    public function getAllActiveMembers()
    {
        try{
            $members = $this->createQueryBuilder('M')
                ->where('M.status = :status')
                ->setParameter('status', 0)
                ->getQuery()
                ->getResult();

            return $members;
        }
        catch (Exception $e){
            throw new Exception('something went wrong');
        }
    }

    public function getMemberById($member_id)
    {
        try{
            return $this->find( $member_id );
        }
        catch (NotFoundHttpException $e){
            throw new NotFoundHttpException('Item not found');
        }
        catch (Exception $e){
            throw new Exception( 'something went wrong error: '.$e->getMessage() );
        }
    }

    public function saveMember(Request $request)
    {
        try {
            if($request->request->get('first_name') == ''){
                return false;
            }
            $member = new Member();
            $member->setFirstName($request->request->get('first_name'));
            $member->setStatus(0);

            $entityManager = $this->getEntityManager();
            $entityManager->persist($member);
            $entityManager->flush();
            return $member;
        }
        catch (Exception $e){
            throw new Exception($e);
        }
    }

    public function updateMember($member_id, Request $request)
    {
        try{

            $member = $this->find( $member_id );
            if( $member && $request->request->get('first_name') )
            {
                $member->setFirstName($request->request->get('first_name'));

                $entityManager = $this->getEntityManager();
                $entityManager->persist( $member );
                $entityManager->flush();
                return $member;



            }
            else {
                return false;
            }
        }
        catch (Exception $e){
            throw new Exception($e);
        }
    }

    public function softDelete($member_id)
    {
        try{
            $member = $this->find( $member_id );
            if( $member ){
                $member->setStatus(1);

                $entityManager = $this->getEntityManager();
                $entityManager->persist( $member );
                $entityManager->flush();
                return true;
            }
            else{
                return false;
            }
        }
        catch (Exception $e){
            throw new Exception('something wrong error: '.$e->getMessage());
        }
    }


}
