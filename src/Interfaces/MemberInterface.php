<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 20/12/2018
 * Time: 20:41
 */

namespace App\Interfaces;



use Symfony\Component\HttpFoundation\Request;

interface MemberInterface
{
    public function getAllActiveMembers();
    public function getMemberById( $member_id );
    public function saveMember( Request $request );
    public function updateMember( $member_id, Request $request );
    public function softDelete( $member_id );

}