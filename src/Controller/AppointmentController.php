<?php

namespace App\Controller;

use App\Entity\Appointment;

use App\Repository\AppointmentRepository;
use App\Repository\MemberRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidatorException;

class AppointmentController extends AbstractController
{
    private $appointmentRepository;
    private $serializer;
    private $memberRepository;
    public function __construct
    (
        AppointmentRepository $appointmentRepository,
        SerializerInterface $serializer,
        MemberRepository $memberRepository
    )
    {
        $this->appointmentRepository = $appointmentRepository;
        $this->serializer = $serializer;
        $this->memberRepository =$memberRepository;
    }

    /**
     * @Route("/appointment", name="appointment", methods={"GET"})
     */
    public function index()
    {
        $appointments = $this->appointmentRepository->getAllAppointments();
        return new Response($this->serializer->serialize($appointments, 'json'));
    }

    /**
     * @Route("/appointment/{id}", name="appointment_by_id", methods={"GET"})
     */
    public function getAppointment( $id ){
        $appointment = $this->appointmentRepository->getAppointmentById( $id );
        if($appointment ){
            return new Response($this->serializer->serialize($appointment, 'json'));
        }
        else{
            return new Response($this->serializer->serialize($appointment, 'json'),404);
        }
    }

    /**
     * @Route("/appointment", name="appointment_create", methods={"POST"})
     */
    public function storeAppointment( Request $request ){
        try{
            $appointment = $this->appointmentRepository->saveAppointment( $request );
            if( $appointment ) {
                return new Response($this->serializer->serialize($appointment, 'json'));
            }
            else{
                return new Response($this->serializer->serialize('wrong date format eg:2018-09-09 09:15:23 ', 'json'), 422);
            }

        }
        catch (Exception $e){
            return new Response($this->serializer->serialize($e->getMessage(), 'json'), $e->getCode() );
        }
    }

    /**
     * @Route("/appointment/{id}", name="appointment_update", methods={"PUT"})
     */
    public function editAppointment( $id, Request $request ){
        $appointment = $this->appointmentRepository->updateAppointment( $id , $request );

        if( $appointment ) {
            return new Response($this->serializer->serialize($appointment, 'json'));
        }
    }

    /**
     * @Route("/appointment/{id}", name="appointment_soft_delete", methods={"DELETE"})
     */
    public function softDelete( $id ){
        $appointment = $this->appointmentRepository->softDelete( $id );
        if( $appointment ) {
            return new Response($this->serializer->serialize('Data deleted', 'json'));
        }
        else{
            return new Response($this->serializer->serialize('Item not found', 'json'),404);
        }
    }


    /**
     * @Route("/appointment/owner", name="appointment_owner_add", methods={"POST"})
     */
    public function addOwner( Request $request ){
        if($request->request->all()){
            $member = $this->memberRepository->find( $request->request->get('memberId'));
            if( $member ){
                $appointment = $this->appointmentRepository->addOwner( $request );
                if( $appointment ) {
                    return new Response($this->serializer->serialize($appointment, 'json'));
                }
                else{
                    return new Response($this->serializer->serialize('Ids invalid', 'json'),404);
                }

            }
            else{
                return new Response($this->serializer->serialize('No valid ids found', 'json'),422);
            }

        }

    }

    public function addParticipants( Request $request ){
        if( $request->request->get('participant')){

        }

    }
}
