<?php

namespace App\Controller;

use App\Repository\MemberRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MemberController extends AbstractController
{
    private $memberRepository;
    private $serializer;

    public function __construct
    (
        MemberRepository $memberRepository,
        SerializerInterface $serializer
    )
    {
        $this->memberRepository = $memberRepository;
        $this->serializer = $serializer;
    }
    /**
     * @Route("/member", name="member", methods={"GET"})
     */
    public function index()
    {
        $members = $this->memberRepository->getAllActiveMembers();
        return new Response($this->serializer->serialize($members, 'json'));
    }

    /**
     * @Route("/member/{id}", name="member_by_id", methods={"GET"})
     */
    public function getMember( $id ){
        $memebr = $this->memberRepository->getMemberById( $id );
        if($memebr ){
            return new Response($this->serializer->serialize($memebr, 'json'));
        }
        else{
            return new Response($this->serializer->serialize($memebr, 'json'),404);
        }
    }


    /**
     * @Route("/member", name="member_create", methods={"POST"})
     */
    public function createMemeber( Request $request ){
        $member = $this->memberRepository->saveMember( $request );
        if( $member ) {
            return new Response($this->serializer->serialize($member, 'json'));
        }
        else{
            return new Response($this->serializer->serialize('name filed can not be emplty', 'json'), 422);
        }

    }

    /**
     * @Route("/member/{id}", name="member_update", methods={"PUT"})
     */
    public function editMember( $id , Request $request ){
        $member = $this->memberRepository->updateMember( $id, $request );

        if( $member ) {
            return new Response($this->serializer->serialize($member, 'json'));
        }
        else{
            return new Response($this->serializer->serialize('not valid fileds', 'json'), 422);
        }

    }

    /**
     * @Route("/member/{id}", name="member_soft_delete", methods={"DELETE"})
     */
    public function softDelete( $id ){
        $member = $this->memberRepository->softDelete( $id );
        if( $member ) {
            return new Response($this->serializer->serialize('Data deleted', 'json'));
        }
        else{
            return new Response($this->serializer->serialize('Item not found', 'json'),404);
        }
    }
}
