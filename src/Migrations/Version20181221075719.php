<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181221075719 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__appointment AS SELECT id, title, created_at, starts_at, ends_at, status, owner FROM appointment');
        $this->addSql('DROP TABLE appointment');
        $this->addSql('CREATE TABLE appointment (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, owner INTEGER DEFAULT NULL, title VARCHAR(255) NOT NULL COLLATE BINARY, created_at DATETIME NOT NULL, starts_at DATETIME NOT NULL, ends_at DATETIME NOT NULL, status SMALLINT DEFAULT NULL, participant VARCHAR(255) DEFAULT NULL, CONSTRAINT FK_FE38F844CF60E67C FOREIGN KEY (owner) REFERENCES member (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO appointment (id, title, created_at, starts_at, ends_at, status, owner) SELECT id, title, created_at, starts_at, ends_at, status, owner FROM __temp__appointment');
        $this->addSql('DROP TABLE __temp__appointment');
        $this->addSql('CREATE INDEX IDX_FE38F844CF60E67C ON appointment (owner)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_FE38F844CF60E67C');
        $this->addSql('CREATE TEMPORARY TABLE __temp__appointment AS SELECT id, owner, title, created_at, starts_at, ends_at, status FROM appointment');
        $this->addSql('DROP TABLE appointment');
        $this->addSql('CREATE TABLE appointment (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, owner INTEGER DEFAULT NULL, title VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, starts_at DATETIME NOT NULL, ends_at DATETIME NOT NULL, status SMALLINT DEFAULT NULL)');
        $this->addSql('INSERT INTO appointment (id, owner, title, created_at, starts_at, ends_at, status) SELECT id, owner, title, created_at, starts_at, ends_at, status FROM __temp__appointment');
        $this->addSql('DROP TABLE __temp__appointment');
    }
}
