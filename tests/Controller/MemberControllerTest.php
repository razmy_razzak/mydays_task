<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MemberControllerTest extends WebTestCase
{
    public function testMemberEndpoint()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/member');
        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function testGetMemberById(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/member/1');
        $data = json_decode( $client->getResponse()->getContent() );
        $this->assertSame(1, $data->id);
    }

    /**
     * As a user I need to get 422 if validation error
     */
    public function testCreateValidationMember(){
        $client = static::createClient();
        $client->request('POST', '/member');
        $this->assertSame(422, $client->getResponse()->getStatusCode());
    }

    /**
     * As A user I would like see the created Member
     */
    public function testCreateMember(){
        $client = static::createClient();
        $client->request('POST', '/member',['first_name' =>'Member_test']);
        $data = json_decode( $client->getResponse()->getContent() );
        $this->assertSame('Member_test', $data->firstName);
    }

    /**
     * As A user I would like to update a record
     */
    public function testUpdateAppointment(){
        $client = static::createClient();
        $client->request('PUT', '/member/1',['first_name' =>'updateMember']);
        $data = json_decode( $client->getResponse()->getContent() );
        $this->assertSame('updateMember', $data->firstName);
    }

    /**
     * As A user I want do soft delete so the Member should't in the list
     */
    public function testsoftDelete(){
        $client = static::createClient();
        $client->request('DELETE', '/member/1');
        $client->request('GET', '/member/1');
        $data = json_decode( $client->getResponse()->getContent() );
        $this->assertSame(1 , $data->status);
    }

}
