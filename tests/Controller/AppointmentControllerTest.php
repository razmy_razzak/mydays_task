<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AppointmentControllerTest extends WebTestCase
{
    public function testAppointmentEndpoint()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/appointment');
        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    /**
     * As a user I need to get 422 if validation error
     */
    public function testCreateValidationAppointment(){
        $client = static::createClient();
        $client->request('POST', '/appointment');
        $this->assertSame(422, $client->getResponse()->getStatusCode());
    }

    /**
     * As A user I would like see the created product
     */
    public function testCreateAppointment(){
        $client = static::createClient();
        $client->request('POST', '/appointment',['title' =>'Test1', 'startsAt' => '2018-12-30 09:15:23', 'endsAt' => '2019-01-04 09:15:23']);
        $data = json_decode( $client->getResponse()->getContent() );
        $this->assertSame('Test1', $data->title);
    }

    /**
     * As A user I would like to update a record
     */
    public function testUpdateAppointment(){
        $client = static::createClient();
        $client->request('PUT', '/appointment/1',['title' =>'updateTest']);
        $data = json_decode( $client->getResponse()->getContent() );
        $this->assertSame('updateTest', $data->title);
    }

    /**
     * As A user I want do soft delete so the product should't in the list
     */
    public function testsoftDelete(){
        $client = static::createClient();
        $client->request('DELETE', '/appointment/1');
        $client->request('GET', '/appointment/1');
        $data = json_decode( $client->getResponse()->getContent() );
        $this->assertSame(1 , $data->status);
    }

    public function addOwner(){
        $client = static::createClient();
        $client->request('POST', '/appointment/owner', ['memberId' =>1,'appointmentId'=> 1]);
        $client->request('GET', '/appointment/1');
        $data = json_decode( $client->getResponse()->getContent() );
        $this->assertSame(1 , $data->owner);
    }


}
